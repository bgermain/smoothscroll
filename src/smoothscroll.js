/**
 * jQuery Smooth Scroll
 * A jQuery Smooth scroll plugin to a target anchor
 */

;
(function($) {
    var pluginName = "smoothScroll",
        defaults = {
            speed: 500,
            offset: 0
        };

    function Plugin(element, options) {
        this._defaults = defaults;
        this._name = pluginName;
        this.element = element;

        this.options = $.extend({}, defaults, options);
        this.clickTap = ('onTap' in window) ? 'tap' : 'click';

        this.bindScrollTo();
    }

    Plugin.prototype = {
        bindScrollTo: function() {
            var speed = this.options.speed,
                offset = this.options.offset;
            if(typeof offset == 'string'){
                offset = ($(offset).length) ? $(offset).height() : 0;
            }
            // get the targeted element to scroll to
            $(this.element).on(this.clickTap, function(e) {
                var $target = $($(this).data('scrollto'));
                e.preventDefault();
                // attach smooth scroll to the element
                $('html, body').animate({
                    scrollTop: ($target.offset().top - offset)
                }, speed);
            });
        }
    };

    $.fn[pluginName] = function(options) {
        return this.each(function() {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName,
                    new Plugin(this, options));
            }
        });
    };

    $.fn[pluginName].Constructor = Plugin;

    // Data API
    $(window).on('load', function() {
        $('[data-scrollto]').each(function() {
            var $spy = $(this),
                data = $spy.data();


            $spy[pluginName](data);
        });
    });

}(window.jQuery || window.Zepto))